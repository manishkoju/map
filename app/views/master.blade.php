<!DOCTYPE html>
<html>
<head>
	<title>Google Map</title>
	<link rel="stylesheet" href="{{asset('css/bootstrap.css')}}">
	<link rel="stylesheet" href="{{asset('css/styles.css')}}">
	<script src="{{asset('js/jquery.js')}}"></script>

	@yield('style')
</head>
<body>
  <div class="text-center container">
  	 @yield('content')
  </div>
  <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB0ATYXPdXeShBNGHHVXSroHvNn1-P7Sc8&libraries=places"
    async defer></script>
    <script src="{{asset('js/javascript.js')}}"></script>

</body>
</html>