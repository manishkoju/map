<?php

class Institute extends \Eloquent {

	// Add your validation rules here
	public static $rules = [
		'name' => 'required',
		'description' => 'required',
		'district' => 'required',
		'city' => 'required',
		'lat' => 'required',
		'lng'=>'required',
	];

	// Don't forget to fill this array
	protected $fillable = ['name','description','district','city','lat','lng'];

}