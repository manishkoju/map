<?php

class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Eloquent::unguard();

		$this->call('UserTableSeeder');
        // $this->call(' InstitutesTableSeeder');
	}

	class UserTableSeeder extends Seeder{

	public function run(){
	$faker=Faker\Factory::create('ne_NP');

	for ($i=0; $i <10; $i++) 
	{ 
	User::create(
			array('name'=>$faker->name,
			'district'=>$faker->district,
				));
		
	 }
	}

  }

}
