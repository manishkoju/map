<?php

// Composer: "fzaninotto/faker": "v1.3.0"
use Faker\Factory as Faker;

class InstitutesTableSeeder extends Seeder {

	public function run()
	{
		$faker = Faker::create('ne_NP');

		foreach(range(1, 10) as $index)
		{
			Institute::create([
			'name'=>$faker->name,
			'district'=>$faker->district,
			'city'=>$faker->city,
			'description'=>$faker->text,
			'lat'=>$faker->latitude,
			'lng'=>$faker->longitude,

			]);
		}
	}

}