var map;

if (navigator.geolocation) {
	navigator.geolocation.getCurrentPosition(success,fail);
}else{
	alert("Browser not supported.Upgrade your browser");
}

function success(position){
	//getting user longtitude and latitude
   
   var userCoords=new google.maps.LatLng(position.coords.latitude,position.coords.longitude);
	//for defining map options

	
	var mapOptions={
		center:userCoords,
		zoom:15
	}
	//creating map object and placte it in div with id map
		map = new google.maps.Map(document.getElementById('map'),mapOptions );
	//create marker for current location
	//custom image icon for marker
	
	var imageIcon="https://developers.google.com/maps/documentation/javascript/examples/full/images/beachflag.png";
	var marker=new google.maps.Marker({
		position:userCoords,
		map:map,
		icon:imageIcon,
		title:'hello map!'
	});

}

function fail(){
	alert("sorry error");
}
